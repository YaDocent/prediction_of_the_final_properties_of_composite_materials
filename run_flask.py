#!/usr/bin/env python
# coding: utf-8

# In[1]:


from flask import Flask, request, render_template
import tensorflow as tf


# In[2]:


app = Flask(__name__)


# In[3]:


@app.route('/')
def choose_prediction_method():
    return render_template('table.html')


# In[4]:


def mn_prediction(params):
    model = tf.keras.models.load_model('model')
    pred = model.predict([params])
    return pred


# In[5]:


@app.route('/', methods=['POST', 'GET'])
def mn_predict():
    message = ''
    if request.method == 'POST':
        param_list = ('dens', 'moe', 'aoh', 'coeg', 'fp', 'sd', 'tm', 'ts', 'rc', 'pa', 'ps', 'pd')
        params = []
        for i in param_list:
            param = request.form.get(i)
            params.append(param)
        params = [float(i.replace(',', '.')) for i in params]

        message = f'Спрогнозированное cоотношение матрица-наполнитель для введенных параметров: {mn_prediction(params)}'
    return render_template('table.html', message=message)


# In[ ]:


if __name__ == '__main__':
    app.run()

